﻿
#include <SFML/Graphics.hpp>
#include <iostream>
#include <windows.h>
#include <chrono>
#include <thread>

using namespace std::chrono_literals;

int main()
{
    setlocale(LC_ALL, "Rus");
    //Создание окна и названия и разрешения 

    sf::RenderWindow window(sf::VideoMode(800, 600), "Snake 4D");

    int shape_x2 = 800, shape_y2 = 0;
    sf::RectangleShape kvadrat2(sf::Vector2f(400.f, 100.f));
    kvadrat2.setPosition(shape_x2, shape_y2);
    kvadrat2.setOrigin(400, 50);
    kvadrat2.setFillColor(sf::Color::Blue);
    
    int shape_x1 = 800, shape_y1 = 300;
    sf::RectangleShape kvadrat1(sf::Vector2f(100.f, 200.f));
    kvadrat1.setPosition(shape_x1, shape_y1);
    kvadrat1.setOrigin(50, 100);
    kvadrat1.setFillColor(sf::Color::Green);
    
    
    int shape_x = 800, shape_y = 600;
    sf::RectangleShape kvadrat(sf::Vector2f(200.f, 200.f));
    kvadrat.setPosition(shape_x, shape_y);
    kvadrat.setOrigin(100, 100);
    kvadrat.setFillColor(sf::Color::Red); 
    

    while (window.isOpen())
    {
        //Переменная собыия
        sf::Event event;

        // цикл по всем событиям
        while (window.pollEvent(event))
        {
            // Обратботка события
            // Если нажат крестик, то
            if (event.type == sf::Event::Closed)
                //окно закрывается
                window.close();
        }

        shape_x--;
        shape_y--;
        if (shape_y < 100) {
            shape_y = 100;
        }
        if (shape_x < 100) {
            shape_x = 100;
        }
        kvadrat.setPosition(shape_x, shape_y);

        shape_x1--;
        if (shape_x1 < 50) {
            shape_x1 = 50;
        }
        kvadrat1.setPosition(shape_x1, shape_y1);

        shape_y2++;
        shape_x2--;
        if (shape_x2 < 400) {
            shape_x2 = 400;
        }
        if (shape_y2 > 550) {
            shape_y2 = 550;
        }
        kvadrat2.setPosition(shape_x2, shape_y2);



        //очитска окна от всего что та есть 
        window.clear();

        // Перемешение фигуры в буфер 
        window.draw(kvadrat);
        window.draw(kvadrat1);
        window.draw(kvadrat2);

        // Отобразить на окне всё что есть в буфере
        window.display();

        std::this_thread::sleep_for(1ms);
    }

    return 0;
}